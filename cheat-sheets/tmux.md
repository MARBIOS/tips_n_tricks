# A tmux Crash Course

[tmux](https://tmux.github.io) is a terminal multiplexer. It lets you switch 
easily between several programs in one terminal, detach them (they keep running 
in the background) and reattach them to a different terminal. And do a lot more.  

If a tmux command I mention is bound to a keyboard shortcut by default, I’ll 
note that in parenthesis.

## Session Management

Sessions are useful for completely separating work environments. You can have a 
‘Project’ session and a 'Developing’ session; in 'Project’, I'd keep everything 
open that I need during my day-to-day bioinformatics analysis for a particular 
project, while in 'Development’, I'd keep open the filess I'm working on for a 
particular program/script.

`tmux new -s session_name`
creates a new tmux session named session_name  
`tmux attach -t session_name`
attaches to an existing tmux session named session_name  
`tmux switch -t session_name`
switches to an existing session named session_name  
`tmux list-sessions`
lists existing tmux sessions  
`tmux detach (prefix + d)`
detach the currently attached session  

## Windows

`tmux` has a tabbed interface, but it calls its tabs “Windows”. To stay organized, 
you can rename all the windows; That way, you can 
recognize windows by context and not what application it’s running.  
break window: `prefix + !` (detaches pane to window)  
merge windows or panes from windows: `tmux join-pane -s (soource) -t (to)`

`tmux new-window (prefix + c)`
create a new window  
`tmux select-window -t :0-9 (prefix + 0-9)`
move to the window based on index  
`tmux rename-window (prefix + ,)`
rename the current window  

## Panes

Panes make easy to have a list of files, `man` page, README file, etc... open in 
one side of the screen while working in the other side...

`tmux split-window (prefix + “)`
splits the window into two vertical panes  
`tmux split-window -h (prefix + %)`
splits the window into two horizontal panes  
`tmux swap-pane -[UDLR] (prefix + { or })`
swaps pane with another in the specified direction  
`tmux select-pane -[UDLR]`
selects the next pane in the specified direction  
`tmux select-pane -t :.+`
selects the next pane in numerical order  

## Helpful tmux commands

`tmux list-keys`
lists out every bound key and the tmux command it runs  
`tmux list-commands`
lists out every tmux command and its arguments  
`tmux info`
lists out every session, window, pane, its pid, etc.  
`tmux source-file ~/.tmux.conf`
reloads the current tmux configuration (based on a default tmux config)  

## Must-haves

These are some of my must-haves in my tmux config. Check my 
[`tmux.conf`](https://gitlab.com/MARBIOS/tips_n_tricks/tree/master/cheat-sheets/tmux.conf) 
file and if you like replace your `~/.tmux.conf` with it:


#### remap prefix to Control + a
	set -g prefix C-a
	unbind C-b
	bind C-a send-prefix

#### force a reload of the config file
	unbind r
	bind r source-file ~/.tmux.conf

#### quick pane cycling
	unbind ^A
	bind ^A select-pane -t :.+


## Workflow

My workflow is something like this:  

I keep at most 3 different sesions open, named significatively to know what I'm 
working on there.  
Generally I divide the main window in 2 panes (left and right) and keep a 
README.md file at the left where I log and document every single thing I do in 
the right pane.  
If I need to edit or write down short scripts (`qsubs` for instance), I open a 
horizontally divided pane on the right side of the screen and edit in the lower 
pane and execute in the upper pane. It saves you to have to close the text editor 
and execute if you need to test things out, for instance.  

When I've finished my work in project A, I `detach` the session and `attach` the 
next one. If I need to go back to the previous one, everything will be as I left 
it before. Nice, eh?  

This *persistency* niciety also allows tmux to ignore the SIGHUP that follows a 
broken `ssh` connection. This means that processes started in a tmux session will 
carry on its execution even though you detach your session or even log out from 
Biocluster.  

**Note that this functionality shouldn't be used in the master node. If you are 
running CPU or memory intensive processes you should have been working 
interactively, or better still, sent it to the queue!**