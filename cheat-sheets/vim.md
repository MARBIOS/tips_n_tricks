# Vi(Vim) cheat-sheet

## Modes

Vi has two modes insertion mode and command mode. 
The editor begins in command mode, where the cursor movement 
and text deletion and pasting occur. Insertion mode begins upon 
entering an insertion or change command. [ESC] returns the editor 
to command mode (where you can quit, for example by typing `:q!`). 
Most commands execute as soon as you type them except for "colon" 
commands which execute when you press the ruturn key.

#### Quitting
|Function|keystroke|
|---|---|
|Exit, saving changes|  `:x`|
|Exit as long as there have been no changes|`:q`|
|Exit and save changes if any have been made|`ZZ`|
|Exit and ignore any changes|`:q!`|
|Save without exit|`:w`|


#### Inserting Text
|Function|keystroke|
|---|---|
|Insert before cursor|`i`|
|Insert before line|`I`|
|Append after cursor|`a`|
|Append after line|`A`|
|Open a new line after current line|`o`|
|Open a new line before current line|`O`|
|Replace one character|`r`|
|Replace many characters|`R`|

#### Motion
|Function|keystroke|
|---|---|
|Move left|`h`|
|Move down|`j`|
|Move up|`k`|
|Move right|`l`|
|Move to next word|`w`|
|Move to next blank delimited word|`W`|
|Move to the beginning of the word|`b`|
|Move to the end of the word|`e`|
|Move to the end of Blank delimited word|`E`|
|Move a sentence back|`(`|
|Move a sentence forward|`)`|
|Move a paragraph back|`{`|
|Move a paragraph forward|`}`|
|Move to the begining of the line|`0`|
|Move to the end of the line|`$`|
|Move to the nth line of the file|`nG`|
|Move to the last line of the file|`G`|
|Move to nth line of the file|`:n`|
|Move forward to c|`fc`|
|Move back to c|`Fc`|
|Move to top of screen|`H`|
|Move to middle of screen|`M`|
|Move to botton of screen|`L`|

#### Deleting text
Almost all deletion commands are performed by typing d followed by a motion. For example, dw deletes a word. A few other deletes are:

|Function|keystroke|
|---|---|
|Delete character to the right of cursor|`x`|
|Delete character to the left of cursor|`X`|
|Delete to the end of the line|`D`|
|Delete current line, (5dd deletes 5 lines...)|`dd`|
|Deletes current line (:d5 deletes 5 lines)|`:d`|
|Deletes to the beginning of the line|`d0`|
|delete to beginning of file # That command can be read as "From line 1 to the current position, delete"|`:1,.d`|
|delete to end of file  # that can be read as "From the current position to the end of file, delete."|`:.,$d`|
|deletes to the next character 'x' (including spacebar)|`dtx`|
|deletes a word, with W meaning a string of any char till a blank space|`dW`|



#### Yanking Text
Like deletion, almost all yank commands are performed by typing y followed by a motion. For example, `y$` yanks to the end of the line. Two other yank commands are:  

|Function|keystroke|
|---|---|
|Yank the current line|`yy`|
|Yank the current line|`:y`|

#### Changing text

The change command is a deletion command that leaves the editor in insert mode. It is performed by typing c followed by a motion. For wxample cw changes a word. A few other change commands are:  

|Function|keystroke|
|---|---|
|Change to the end of the line|`C`|
|Change the whole line|`cc`|

#### Putting text
|Function|keystroke|
|---|---|
|Put after the position or after the line|`p`|
|Put before the position or before the line|`P`|
