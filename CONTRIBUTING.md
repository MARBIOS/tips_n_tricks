# How to contribute to this repository?

## "Dependencies"  
First of all, if you are not familiar with `Git` or version control, please visit any of these links:  
- [Dr. Chimp's Git Begginer's guide for dummies](https://backlogtool.com/git-guide/en/intro/intro1_1.html).  
- [Git's](https://git-scm.com/doc) documentation page: `man` pages, [Pro Book](https://git-scm.com/book/en/v2), cheat-sheets and videos.  
- [Seth's Robertson Git best practices](https://sethrobertson.github.io/GitBestPractices/)  

This documentation is written in `markdown` the **super-easy** text-to-HTML tool 
that you can learn in 10 minutes [here](https://gitlab.com/MARBIOS/tips_n_tricks/wikis/markdown-for-beginners).  

## Aims of a `Git` repository
The aims of git repositories around Biocluster are that no one would ask again something like 
*"Where are the files of [...]"*, or *"Is this [...] already done?"*, or *"Who 
has this piece of information?"*, or *"Is this the most up-to-date version of file X?"*, nor
*"Is this program installed at biocluster?"* by keeping a detailed [documentation](https://gitlab.com/MARBIOS/biocluster-info/wikis/home)
in the shape of a wiki website.
...and if those questions arise, use the [issue tracker](https://gitlab.com/MARBIOS/biocluster-info/issues) 
to let everybody know that there is a *blank* in the common knowledge and ask soemone to fix it.  

## Contributing
This is a collaborative repository, meaning that you are more than welcome to add, 
edit, correct, improve or chriticize everything that is posted here. You'll need 
to be a member of this group to do it, so the first thing is to sign up and then ask @pablo.sanchez
to add your username to the group.  

You can add or edit content via this website, but this way is discouraged. It is preferred 
that you `clone` this repo in your local computer and commit and push your changes via `git`. 

#### Contributing via web
In this web you can  
- read the documentation [wiki](https://gitlab.com/MARBIOS/biocluster-info/wikis/home)  
- Post issues with the [issue tracker](https://gitlab.com/MARBIOS/biocluster-info/issues).  

Moreover, you can 
- do edits to wiki pages and `README` files, etc.  
- upload new files  
- replace older files with new versions  

**BUT** if you have read about the `Git` philosophy above, notice that if two 
people are editing the same file on-line, changes added by one user while another 
one is editing may be undone by the latter when saving her work.  

#### Contributing via `Git` in the terminal  
This is the preferred way to collaborate in this repo (and other GitLab repos), 
besides posting issues in the website.  
Clone the repository in your local computer (or at some place you own at the 
biocluster) and start working in your particular piece of the project.  

`Commit` often, and if you are collaborating with someone else in your part of 
the project, `pull` often, fix conflicts locally and `push` only when you are ready.

### What could I possibly contribute with?
You can add lots of things to the *common knowledge*! Just be organized and 
everything will be fine...  

- For instance, if you read a documentation page that it's not clear you can post an **issue** 
asking for a clarification or, if you think you could do better, simply edit the page yourself!  
- If you have a nice bioinformatics *kung fu* trick, post it in the [tips'n'tricks](https://gitlab.com/MARBIOS/tips_n_tricks/wikis/Home) 
wiki pages.  
- If you have made a **cheat-sheet** summarizing dark commands of an R 
package or terminal text editor, you can post it within the [cheat-sheets](https://gitlab.com/MARBIOS/tips_n_tricks/wikis/cheat-sheets) 
section, etc...  
- If you have some piece of wisdom to share that doesn't fit in any of the seections that
exist already, create a new one!  

Just be **organized**! Put your information within the proper MARBIOS group project 
(i.e. [tips'n'tricks](https://gitlab.com/MARBIOS/tips_n_tricks/wikis/Home), 
[Biocluster](https://gitlab.com/MARBIOS/biocluster-info/wikis/home), or any new project that may be created in the future) 
Let the Home page of the different wikis be an *index*, 
look for a section that fits to add your new content and if it doesn't exist, create 
a new one. Be **very liberal** with your linking, as one page needs to lead to another 
to build a wisdom-continuum. Also don't be afraid to create as many wiki pages as you need, 
provided that you build the proper links between them.  

Creating wiki pages is easy and you can make them look gorgeous using 
[`markdown`](https://gitlab.com/MARBIOS/tips_n_tricks/wikis/markdown-for-beginners).

## Collaborate!
Help improve this documentation and all the documentation around this repository.
If you are afraid to mess up (don't be), post an issue and start collaborating!