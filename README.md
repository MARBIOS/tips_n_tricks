# Tips'n'tricks
Here you can find useful one-liners in bash, perl, etc. as well as common workflows 
that you can use for your daily bioinformatics jobs.  

- [**Wiki**](https://gitlab.com/MARBIOS/tips_n_tricks/wikis/Home). Everything is in here!
- [**Browse issues**](https://gitlab.com/MARBIOS/tips_n_tricks/issues). See what other users are discussing
- [**Open new issue**](https://gitlab.com/MARBIOS/tips_n_tricks/issues/new). Start your own discussion/petition/phylosophical question here  

And, of course, you can use the search box in the upper right corner.  

Help us improving this documentation! But read the [contribution guidelines](CONTRIBUTING.md) first...